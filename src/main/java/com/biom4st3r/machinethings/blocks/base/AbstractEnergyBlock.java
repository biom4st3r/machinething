package com.biom4st3r.machinethings.blocks.base;

import java.util.Collections;
import java.util.Set;

import nerdhub.cardinal.components.api.BlockComponentProvider;
import nerdhub.cardinal.components.api.ComponentType;
import nerdhub.cardinal.components.api.component.Component;
import nerdhub.cardinalenergy.DefaultTypes;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;


public abstract class AbstractEnergyBlock extends BlockWithEntity implements BlockComponentProvider {

    public AbstractEnergyBlock(Settings block$Settings_1,int capacity,int txrate,int rxrate,BlockEntityType<?> type) {
        super(block$Settings_1);
        this.capacity = capacity;
        this.txRate = txrate;
        this.rxRate = rxrate;
        this.entityType = type;
    }

    
    protected  int capacity;
    protected int txRate;
    protected int rxRate;
    public BlockEntityType<?> entityType;


    protected <T extends AbstractEnergyBlockEntity> T createEntity()
    {
        T ff = (T) entityType.instantiate();
        ff.getEPool().capacity = this.capacity;
        ff.getEPool().receiveRate = this.rxRate;
        ff.getEPool().sendRate = this.txRate;
        ff.type = entityType;
        return ff;
        
    }

    @Override
    public boolean activate(BlockState blockState_1, World world, BlockPos blockPos_1, PlayerEntity playerEntity_1,Hand hand_1, BlockHitResult blockHitResult_1) {
        if(!world.isClient)
        {
            if(playerEntity_1.getStackInHand(hand_1) == ItemStack.EMPTY)
            {
                AbstractEnergyBlockEntity aebe = (AbstractEnergyBlockEntity)world.getBlockEntity(blockPos_1);
                playerEntity_1.sendMessage(new TextComponent(String.format("%s / %s Energy Stored", aebe.getEPool().getEnergyStored(),aebe.getEPool().getCapacity())));
                return true;
            }
        }
        return super.activate(blockState_1, world, blockPos_1, playerEntity_1, hand_1, blockHitResult_1);
    }

    @Override
    public <T extends Component> T getComponent(BlockView blockView, BlockPos pos, ComponentType<T> type,
            Direction side) {
            return type == DefaultTypes.CARDINAL_ENERGY ? (T) ((AbstractEnergyBlockEntity)blockView.getBlockEntity(pos)).getEPool() : null;
    }

    @Override
    public Set<ComponentType<? extends Component>> getComponentTypes(BlockView blockView, BlockPos pos,
            Direction side) {
                return Collections.singleton(DefaultTypes.CARDINAL_ENERGY);
    }

    @Override
    public <T extends Component> boolean hasComponent(BlockView blockView, BlockPos pos, ComponentType<T> type,
            Direction side) {
            return type == DefaultTypes.CARDINAL_ENERGY;
    }



    @Override
    public BlockEntity createBlockEntity(BlockView arg0)
    {
        return createEntity();
    }

    @Override
    public BlockRenderType getRenderType(BlockState blockState_1) {
        return BlockRenderType.MODEL;
    }

    // @Override
    // protected abstract void appendProperties(StateFactory.Builder<Block, BlockState> stateFactory$Builder_1);

    // @Override
    // public abstract BlockState getPlacementState(ItemPlacementContext itemPlacementContext_1);


    // @Override
    // protected void appendProperties(StateFactory.Builder<Block, BlockState> stateFactory$Builder_1) {
    //     stateFactory$Builder_1.add(new Property[]{FACING});
    // }
    // @Override
    // public BlockState getPlacementState(ItemPlacementContext itemPlacementContext_1) {
    //     return (BlockState)this.getDefaultState().with(FACING, itemPlacementContext_1.getPlayerFacing().getOpposite());
    // }

}