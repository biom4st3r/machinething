package com.biom4st3r.machinethings.blocks.base;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import com.biom4st3r.machinethings.Util;
import com.biom4st3r.machinethings.blocks.EnergyPool;
import com.biom4st3r.machinethings.registers.BlockRegisters;

import nerdhub.cardinal.components.api.BlockComponentProvider;
import nerdhub.cardinal.components.api.ComponentType;
import nerdhub.cardinalenergy.DefaultTypes;
import nerdhub.cardinalenergy.api.IEnergyHandler;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Identifier;
import net.minecraft.util.Tickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

public abstract class AbstractEnergyBlockEntity extends BlockEntity
        implements IEnergyHandler, Tickable {

    
    public BlockEntityType<?> type;

    protected Direction lastDirection = Direction.DOWN;

    public AbstractEnergyBlockEntity(BlockEntityType<?> blockEntityType_1, int capacity, int receiveRate,
            int sendRate) {
        super(blockEntityType_1);
        this.ePool = new EnergyPool(capacity, 0, receiveRate, sendRate);
        if(lastDirection == null)
        {
            lastDirection = Direction.byId(0);
        }

        //World
        //ClientWorld
        //ServerWorld
    
    }

    @Override
    public boolean canConnectEnergy(Direction direction, ComponentType componentType) {
        this.lastDirection = direction;
        return IEnergyHandler.super.canConnectEnergy(direction, componentType);
    }

    @Override
    public boolean isEnergyReceiver(Direction direction, ComponentType componentType) {
        this.lastDirection = direction;
        //Util.debug(direction.toString());
        return IEnergyHandler.super.isEnergyReceiver(direction, componentType);
    }

    @Override
    public BlockEntityType<?> getType()
    {
        return this.type;
    }

    public AbstractEnergyBlockEntity(BlockEntityType<?> blockEntityType) {
        super(blockEntityType);
        this.ePool = new EnergyPool(0, 0, 0, 0);
    }

    protected EnergyPool ePool;

    public EnergyPool getEPool()
    {
        return ePool;
    }

    public void fromTag(CompoundTag tag) 
    {
        this.type = BlockRegisters.typeLookup.get(new Identifier(tag.getString("id")));
        super.fromTag(tag);
        try
        {
            this.lastDirection = Direction.byId(tag.getInt("ld"));
        }
        catch(NullPointerException e)
        {
            this.lastDirection = Direction.SOUTH;
        }

        this.ePool.readEnergyFromTag(tag);
    }

    public CompoundTag toTag(CompoundTag tag) 
    {
        super.toTag(tag);
        try
        {
            tag.putInt("ld", this.lastDirection.ordinal());
        }
        catch(NullPointerException e)
        {
            tag.putInt("ld",3);
        }

        this.ePool.writeEnergyToTag(tag);
        return tag;
    }

    @Override
    public abstract void tick();

    // private static <T> List<T> removeAR(List<T> l, T object) 
    // {
    //     l.remove(object);
    //     return l;
    // }

    // private static <T> List<T> addAR(List<T> l,T object)
    // {
    //     l.add(object);
    //     return l;
    // }

    protected void sendEnergyToNeightbors()
    {
        if(!this.world.isClient) 
        {
            if(this.ePool.getEnergyStored() >= this.ePool.sendRate)
            {
                //List<IEnergyHandler> neighbors = new ArrayList<IEnergyHandler>(6);
                //Direction[] ff = Direction.values().clone();

                int[] directions = new int[] {0,1,2,3,4,5};

                if(this.lastDirection != null)
                {
                    Util.debug(this.lastDirection.toString());
                    directions[this.lastDirection.ordinal()] = 5;
                    directions[5] = this.lastDirection.ordinal();
                    //Util.debug(String.format("%s, %s, %s, %s, %s, %s", Direction.byId(directions[0]),Direction.byId(directions[1]),Direction.byId(directions[2]),Direction.byId(directions[3]),Direction.byId(directions[4]),Direction.byId(directions[5])));
                }
                for(Integer i : directions)
                {
                   // Util.debug(Direction.byId(i));
                    Direction dir = Direction.byId(i);
                    BlockPos neighbor = this.pos.offset(dir);
                    //BlockComponentProvider componentProvider = (BlockComponentProvider)world.getBlockState(pos).getBlock();
                    BlockEntity neighborEntity = world.getBlockEntity(neighbor);
                    IEnergyHandler energyNeighbor = neighborEntity instanceof IEnergyHandler ? (IEnergyHandler) neighborEntity : null;
                    if(energyNeighbor != null &&
                        energyNeighbor.isEnergyReceiver(dir.getOpposite(), DefaultTypes.CARDINAL_ENERGY))
                    {
                        //Util.debug(this.lastDirection);
                        Util.debug(String.format("%s,%s,%s energizing Neightbor", this.getPos().getX(),this.getPos().getY(), this.getPos().getZ()));
                        //Util.debug(String.format("%s %s %s %s", this.ePool.energyStored, this.ePool.capacity,this.ePool.receiveRate,this.ePool.sendRate));
                        int energySent = this.ePool.sendEnergy(this.world, neighbor, this.ePool.sendRate);
                        //neighbors.add(energyNeighbor);

                        //TODO FIX ME :D! I don't do shit
                        //TODO the fuck? update everyPlayer on the server?https://github.com/NerdHubMC/Refined-Machinery/blob/master/src/main/java/abused_master/refinedmachinery/utils/EnergyHelper.java
                    }
                }
            }
        }
    }

}