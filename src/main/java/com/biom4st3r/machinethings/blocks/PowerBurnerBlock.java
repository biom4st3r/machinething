package com.biom4st3r.machinethings.blocks;

import com.biom4st3r.machinethings.Util;
import com.biom4st3r.machinethings.blocks.base.AbstractEnergyBlock;

import net.minecraft.block.entity.BlockEntityType;

public class PowerBurnerBlock extends AbstractEnergyBlock {

    public PowerBurnerBlock(Settings block$Settings_1, int capacity, int txrate, int rxrate, BlockEntityType<?> type) {
        super(block$Settings_1, Util.capacityTeir*1,  Util.rateTeir*1, Util.rateTeir*1, type);
    }
}