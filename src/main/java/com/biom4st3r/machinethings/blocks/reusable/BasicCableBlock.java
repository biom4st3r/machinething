package com.biom4st3r.machinethings.blocks.reusable;

import com.biom4st3r.machinethings.blocks.base.AbstractEnergyBlock;

import nerdhub.cardinalenergy.DefaultTypes;
import nerdhub.cardinalenergy.api.IEnergyHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderLayer;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.EntityContext;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.state.StateFactory;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.IWorld;

public class BasicCableBlock extends AbstractEnergyBlock 
{


    public static final BooleanProperty[] properties = new BooleanProperty[]
    {
        BooleanProperty.of("down"),
        BooleanProperty.of("up"),
        BooleanProperty.of("north"),
        BooleanProperty.of("south"),
        BooleanProperty.of("west"),
        BooleanProperty.of("east"),
        BooleanProperty.of("none")
    };

    public BasicCableBlock(Settings block$Settings_1,int capacity, int txrate, int rxrate, BlockEntityType<?> type) {
        super(block$Settings_1,capacity,txrate,rxrate,type);
        this.setDefaultState(this.stateFactory.getDefaultState().with(properties[6], true));

        // for(Property p : this.stateFactory.getDefaultState().getProperties())
        // {
        //     System.out.println(p.getName() + " " + p.getValues());
        // }
    }

    @Override
    public BlockState getPlacementState(ItemPlacementContext ipc)
    {
        return findPlacementStates(ipc.getWorld(), this.getDefaultState(), ipc.getBlockPos());
    }

    @Override
    public BlockState getStateForNeighborUpdate(BlockState blockState_1, Direction direction_1, BlockState blockState_2,
            IWorld iWorld_1, BlockPos blockPos_1, BlockPos blockPos_2) {
        return findPlacementStates(iWorld_1, blockState_1, blockPos_1);
    }

    public BlockState findPlacementStates(IWorld w, BlockState blockState, BlockPos blockPos)
    {
        for(Direction d : Direction.values())
        {
            BlockPos neightbor = blockPos.offset(d);
            if(w.getBlockEntity(neightbor) instanceof IEnergyHandler &&
             ((IEnergyHandler) w.getBlockEntity(neightbor)).canConnectEnergy(d.getOpposite(), DefaultTypes.CARDINAL_ENERGY))
            {
                blockState = blockState.with(properties[d.getId()], true);
            }
            else
            {
                blockState = blockState.with(properties[d.getId()],false);
            }
        }
        return blockState;
    }

    @Override
    public void appendProperties(StateFactory.Builder<Block, BlockState> stateBuilder) {
        super.appendProperties(stateBuilder.add(properties));
    }

    @Override
    public BlockRenderLayer getRenderLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }

    @Override
    public boolean isSimpleFullBlock(BlockState blockState_1, BlockView blockView_1, BlockPos blockPos_1) {
        return false;
    }

    @Override
    public VoxelShape getOutlineShape(BlockState blockState_1, BlockView blockView_1, BlockPos blockPos_1,
            EntityContext entityContext_1) {
        return Block.createCuboidShape(5, 5, 5, 11, 11, 11);
    }


    
}