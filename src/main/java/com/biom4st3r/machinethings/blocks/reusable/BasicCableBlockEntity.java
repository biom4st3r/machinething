package com.biom4st3r.machinethings.blocks.reusable;

import com.biom4st3r.machinethings.blocks.base.AbstractEnergyBlockEntity;

import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

public class BasicCableBlockEntity extends AbstractEnergyBlockEntity {

    public BasicCableBlockEntity(BlockEntityType<?> blockEntityType_1, int capacity, int receiveRate, int sendRate) {
        super(blockEntityType_1, capacity, receiveRate, sendRate);
    }
    public BasicCableBlockEntity()
    {
        super((BlockEntityType<?>)null);
        //this.getType();
    }

    public BasicCableBlockEntity(int capacity,int receiveRate, int sendRate)
    {
        this((BlockEntityType<?>)null,capacity,receiveRate,sendRate);
    }

    @Override
    public void tick() {
        this.sendEnergy();
        //this.sendEnergyToNeightbors();
    }

    public void sendEnergy()
    {
        for(Direction d : Direction.values())
        {
            BlockPos neightbor = this.pos.offset(d);
            this.getEPool().sendEnergy(this.world, neightbor, this.ePool.sendRate);
        }
    }
    
}