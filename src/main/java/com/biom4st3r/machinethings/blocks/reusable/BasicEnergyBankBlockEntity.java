package com.biom4st3r.machinethings.blocks.reusable;

import com.biom4st3r.machinethings.blocks.base.AbstractEnergyBlockEntity;

import net.minecraft.block.entity.BlockEntityType;

public class BasicEnergyBankBlockEntity extends AbstractEnergyBlockEntity 
{
    public BasicEnergyBankBlockEntity(BlockEntityType<?> blockEntityType_1, int capacity, int receiveRate, int sendRate) {
        super(blockEntityType_1, capacity, receiveRate, sendRate);
    }

    public BasicEnergyBankBlockEntity()
    {
        super((BlockEntityType<?>)null);
    }
    public BasicEnergyBankBlockEntity(int capacity, int receiveRate, int sendRate)
    {
        this((BlockEntityType<?>)null,capacity,receiveRate,sendRate);
    }

    @Override
    public void tick() {
        this.sendEnergyToNeightbors();
    }


}