package com.biom4st3r.machinethings.blocks;

import com.biom4st3r.machinethings.Util;
import com.biom4st3r.machinethings.blocks.base.AbstractEnergyBlockEntity;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntityType;

public class PowerBurnerBlockEntity extends AbstractEnergyBlockEntity {

    boolean lit = false;

    public PowerBurnerBlockEntity(BlockEntityType<?> blockEntityType) {
        super(blockEntityType);
    }

    public PowerBurnerBlockEntity(BlockEntityType<?> blockEntityType_1, int capacity, int receiveRate,
            int sendRate) {
        super(blockEntityType_1);
        this.ePool = new EnergyPool(capacity, 0, receiveRate, sendRate);

        //World
        //ClientWorld
        //ServerWorld
        //CommandBlockBlockEntity
    
    }

    public PowerBurnerBlockEntity()
    {
        super((BlockEntityType<?>)null);
    }

    @Override
    public void tick() {
        
        lit = this.ePool.extractEnergy(10) > 0;
        

    }
    
}