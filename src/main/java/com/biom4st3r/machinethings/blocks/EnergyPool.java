package com.biom4st3r.machinethings.blocks;

import com.biom4st3r.machinethings.Util;

import nerdhub.cardinal.components.api.BlockComponentProvider;
import nerdhub.cardinalenergy.DefaultTypes;
import nerdhub.cardinalenergy.api.IEnergyHandler;
import nerdhub.cardinalenergy.api.IEnergyStorage;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class EnergyPool implements IEnergyStorage {
    public static final String CAPACITY = "capacity";
    public static final String ENERGY_STORE = "energystore";
    public static final String RECEIVE_RATE = "rxrate";
    public static final String SEND_RATE = "txrate";
    public int capacity;
    public int energyStored;
    public int receiveRate;
    public int sendRate;

    public EnergyPool(int capacity, int energyStored,int receiveRate, int sendRate)
    {
        this.capacity = capacity;
        this.energyStored = energyStored;
        this.receiveRate = receiveRate;
        this.sendRate = sendRate;
    }

    @Override
    public int receiveEnergy(int amount) {
        if (amount < 0) {
            System.out.println(new Exception().getStackTrace());
            Util.debug("receiveEnergy amount must be positive");
            return 0;
        }
        final int initStore = energyStored;
        int normalizedAmount = Math.min(amount, receiveRate);
        int newStore = energyStored + normalizedAmount;
        energyStored = Math.min(newStore,capacity);
        //Util.debug(energyStored - initStore);
        return energyStored - initStore;
    }

    @Override
    public int extractEnergy(int amount) {
        if (amount < 0) {
            System.out.println(new Exception().getStackTrace());
            Util.debug("extractEnergy amount must be positive");
            return 0;
        }
        final int initStore = energyStored;
        int normalizedAmount = Math.min(amount, sendRate);
        int newStore = energyStored - normalizedAmount;
        energyStored = Math.max(0,newStore);//(newStore < 0 ? 0 : energyStored - normalizedAmount);
        return initStore - energyStored;
    }

    @Override
    public boolean canReceive(int amount) {
        return energyStored + amount <= capacity;
    }

    @Override
    public boolean canExtract(int amount) {
        return energyStored - amount >= 0;
    }

    @Override
    public void setEnergyStored(int amount) {
        int newStore = energyStored + amount;
        energyStored = (newStore > capacity ? capacity : newStore);
    }

    @Override
    public void setCapacity(int amount) {
        Util.debug("All capsities are Final");
    }

    @Override
    public int getEnergyStored() {
        return energyStored;
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public int sendEnergy(World world, BlockPos pos, int amount) {
        if (amount <= energyStored) {
            Util.debug("Sending Energy");
            if (world.getBlockEntity(pos) instanceof IEnergyHandler) {
                int amountReceived = getEnergyReceiver(world, pos).receiveEnergy(amount);
                this.extractEnergy(amountReceived);
                return amountReceived;
            }
        }
        return 0;
    }

    public IEnergyStorage getEnergyReceiver(World world, BlockPos pos) {
        BlockComponentProvider componentProvider = (BlockComponentProvider) world.getBlockState(pos).getBlock();

        if (world.getBlockEntity(pos) instanceof IEnergyHandler
                && componentProvider.hasComponent(world, pos, DefaultTypes.CARDINAL_ENERGY, null)) {
            IEnergyHandler energyHandler = (IEnergyHandler) world.getBlockEntity(pos);
            return energyHandler.canConnectEnergy(null, DefaultTypes.CARDINAL_ENERGY)
                    ? componentProvider.getComponent(world, pos, DefaultTypes.CARDINAL_ENERGY, null)
                    : null;
        }
        return null;
    }

    @Override
    public CompoundTag writeEnergyToTag(CompoundTag tag) {
        tag.putInt(CAPACITY, capacity);
        tag.putInt(ENERGY_STORE, energyStored);
        tag.putInt(RECEIVE_RATE, receiveRate);
        tag.putInt(SEND_RATE,sendRate);
        return tag;
    }

    @Override
    public void readEnergyFromTag(CompoundTag tag) {
        capacity = tag.getInt(CAPACITY);
        energyStored = tag.getInt(ENERGY_STORE);
        sendRate = tag.getInt(SEND_RATE);
        receiveRate = tag.getInt(RECEIVE_RATE);
    }
}