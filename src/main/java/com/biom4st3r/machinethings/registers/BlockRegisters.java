package com.biom4st3r.machinethings.registers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.function.Supplier;

import com.biom4st3r.machinethings.Util;
import com.biom4st3r.machinethings.blocks.PowerBurnerBlock;
import com.biom4st3r.machinethings.blocks.PowerBurnerBlockEntity;
import com.biom4st3r.machinethings.blocks.reusable.*;
import com.google.common.collect.Maps;

import net.fabricmc.fabric.api.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.RedstoneBlock;
import net.minecraft.block.RedstoneLampBlock;
import net.minecraft.block.TorchBlock;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class BlockRegisters 
{

    public static final Map<Identifier,BlockEntityType<?>> typeLookup = Maps.newHashMap();

    public static void blockEntityReg(String identifier,BlockEntityType<?> type)
    {
        Identifier id = new Identifier(Util.MODID,identifier.toLowerCase());
        BlockEntityType<?> b = Registry.register(Registry.BLOCK_ENTITY, id, type);
        typeLookup.put(id, b);
    }
    public static <T extends BlockEntity> BlockEntityType<T> blockEntityCreate(Supplier<T> supplier,Block... blocks)
    {
        return new BlockEntityType<T>(supplier,new HashSet<Block>(Arrays.asList(blocks)),null);
    }

    public static void blockReg(String identifier,Block block)
    {
        Identifier id = new Identifier(Util.MODID,identifier.toLowerCase());
        Registry.register(Registry.BLOCK, id, block);
    }

    public static void blockItemReg(String identifier,Block block, Item.Settings settings)
    {
        Identifier id = new Identifier(Util.MODID,identifier.toLowerCase());
        Registry.register(Registry.ITEM, id, new BlockItem(block, new Item.Settings().group(ItemGroup.MISC)));
        //Registry.register(Registry.BLOCK, id, block);
        blockReg(identifier, block);
    }
    

    public static void entityBlockItemReg(String identifier,Block block,Item.Settings settings,BlockEntityType<?> type)
    {
        blockEntityReg(identifier, type);
        blockItemReg(identifier, block, settings);
    }


    public static final BasicEnergyBankBlock t1CapacitorBlock;
    public static final BlockEntityType<BasicEnergyBankBlockEntity> t1CapacitorBlockEntityType;
    public static final BasicCableBlock t1CableBlock;
    public static final BlockEntityType<BasicCableBlockEntity> t1CableBlockEntityType;
    public static final BasicCableBlock spruceCableBlock;
    public static final BlockEntityType<BasicCableBlockEntity> spruceCableBlockEntityType;

    public static final PowerBurnerBlock powerburner;
    public static final BlockEntityType<PowerBurnerBlockEntity> powerburnerEntityType;


    private static Block.Settings cableSettings = FabricBlockSettings.copy(Blocks.TORCH).sounds(BlockSoundGroup.STONE).lightLevel(0).build();
    
    static
    {
        
        t1CapacitorBlock = new BasicEnergyBankBlock(
            FabricBlockSettings.copy(Blocks.STONE).build(),
            Util.capacityTeir*1,
            Util.rateTeir,
            Util.rateTeir,
            null);
        t1CapacitorBlockEntityType = blockEntityCreate(BasicEnergyBankBlockEntity::new, t1CapacitorBlock);
        t1CapacitorBlock.entityType = t1CapacitorBlockEntityType;

        spruceCableBlock = new BasicCableBlock(
            FabricBlockSettings.copyOf(cableSettings).build(),
            Util.rateTeir*2,
            Util.rateTeir*2,
            Util.rateTeir*2,
            null);
        spruceCableBlockEntityType = blockEntityCreate(BasicCableBlockEntity::new, spruceCableBlock);
        spruceCableBlock.entityType = spruceCableBlockEntityType;

        t1CableBlock = new BasicCableBlock(
            FabricBlockSettings.copyOf(cableSettings).build(),
            Util.rateTeir,
            Util.rateTeir,
            Util.rateTeir,
            null);
        t1CableBlockEntityType = blockEntityCreate(BasicCableBlockEntity::new, t1CableBlock);
        t1CableBlock.entityType = t1CableBlockEntityType;

        powerburner = new PowerBurnerBlock(FabricBlockSettings.copy(Blocks.STONE).build(), 0, 0, 0, null);
        powerburnerEntityType = blockEntityCreate(PowerBurnerBlockEntity::new,powerburner);
        powerburner.entityType = powerburnerEntityType;

    }
    public static void init()
    {

        entityBlockItemReg("T1BatteryBlock", t1CapacitorBlock, new Item.Settings().group(ItemGroup.MISC),t1CapacitorBlockEntityType);

        entityBlockItemReg("sprucecableblock", spruceCableBlock, new Item.Settings().group(ItemGroup.MISC),spruceCableBlockEntityType);

        entityBlockItemReg("t1CableBlock", t1CableBlock, new Item.Settings().group(ItemGroup.MISC),t1CableBlockEntityType);

        entityBlockItemReg("powerburner", powerburner, new Item.Settings().group(ItemGroup.MISC), powerburnerEntityType);

    }
}
