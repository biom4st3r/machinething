package com.biom4st3r.machinethings;

import com.biom4st3r.machinethings.registers.BlockRegisters;

import net.fabricmc.api.ModInitializer;

public class MachineThings implements ModInitializer
{

    @Override
    public void onInitialize() {
        BlockRegisters.init();
    }
    
}