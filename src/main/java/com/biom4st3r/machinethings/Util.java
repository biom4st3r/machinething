package com.biom4st3r.machinethings;

public class Util
{
    public static final String MODID = "biom4st3rmts";
    public static final String MODID_Short = "b43rmts";

    static boolean isDebug = true;

    public static void debug(Object o)
    {
        if(isDebug)
            System.out.println(String.format("%s: %s", MODID_Short,o.toString()));
    }

    public static int capacityTeir = 5000;
    public static int rateTeir = 20;

    // int[] capacityTeir = new int[] 
    // {5000,10000};

    // int[] rateTeir = new int[] 
    // {20,40,};
}